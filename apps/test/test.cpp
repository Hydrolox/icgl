#include "icgl/icgl_state.h"
#include "objload.h"
#include "parse.h"
#include <app_support/as_window.h>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <glad/glad.h>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <icgl/icgl_draw.h>
#include <icgl/icgl_shader.h>
#include <map>
#include <string_view>
#define SDL_MAIN_HANDLED
#include <SDL.h>

const char vs[] =
 R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
out vec3 vs_color;

uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4( vertex_position, 1.0 );
    vs_color = normalize( vertex_position ) + 1;
})";
const char fs[] =
 R"(#version 330 core

    in vec3 vs_color;
    out vec3 color;

    void main()
    {
        //color = vec3( 1, 0, 0 );
        color = vs_color;
    }
    )";

const char lit_object_vs[] =
 R"(#version 330 core

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
out vec3 vs_color;
out vec3 vs_normal;
out vec3 vs_frag_pos;

uniform mat4 model;
uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4( vertex_position, 1.0 );
    vs_frag_pos = vec3( model * vec4( vertex_position, 1.0 ) );
    vs_color = normalize( vertex_position ) + 1;
    vs_normal = vertex_normal;
})";

const char lit_object_fs[] =
 R"(#version 330 core

    in vec3 vs_normal;
    in vec3 vs_frag_pos;
    out vec4 color;

    uniform vec4 object_color;
    uniform vec4 light_color;
    uniform vec4 light_pos;

    void main()
    {
        float ambient_strength = 1.0;
        vec3 ambient = ambient_strength * vec3( light_color );

        vec3 norm = normalize( vs_normal );
        vec3 light_dir = normalize( vec3( light_pos ) - vs_frag_pos );
        float diff = max( dot( norm, light_dir ), 0.0 );
        vec3 diffuse = diff * vec3( light_color );
        //color = vec4( light_dir.xyz, 1.0 );
        color = vec4( (ambient + diffuse) * vec3( object_color ), 1.0 );
        //color = light_color * object_color;
    }
 )";

const char light_vs[] =
 R"(#version 330 core
    layout (location = 0) in vec3 vertex_position;

    uniform mat4 MVP;

    void main()
    {
        gl_Position = MVP * vec4( vertex_position, 1.0 );
    }
 )";

const char light_fs[] =
 R"(#version 330 core
    out vec4 color;

    void main()
    {
        color = vec4(1.0);
    }
 )";

const char sdef_source[] =
 R"(meshref mesh='michka.obj' pos={20.0,0.0,0.0};)";

struct camera
{
    float field_of_view_degrees{45.0f};
    float speed{1.5f};
    float mouse_speed{0.3f};
    float horiz_angle_degrees{180.0f};
    float vert_angle_degrees{0.0f};
    glm::vec3 position{ 0.0f, 0.0f, 5.0f};
    glm::vec3 right{};
    glm::vec3 direction{};
    glm::vec3 up{};
    glm::mat4 projection_matrix{};
    glm::mat4 view_matrix{};
};

glm::vec3 process_camera_move_event( const camera& a_camera,
                                     const SDL_KeyboardEvent& a_ev,
                                     float a_delta_time )
{
    glm::vec3 delta_pos{};
    if( a_ev.keysym.sym == SDLK_w )
        delta_pos += a_camera.direction * a_delta_time * a_camera.speed;
    if( a_ev.keysym.sym == SDLK_s )
        delta_pos -= a_camera.direction * a_delta_time * a_camera.speed;
    if( a_ev.keysym.sym == SDLK_d )
        delta_pos += a_camera.right * a_delta_time * a_camera.speed;
    if( a_ev.keysym.sym == SDLK_a )
        delta_pos -= a_camera.right * a_delta_time * a_camera.speed;
    return delta_pos;
}

constexpr auto window_width = 800;
constexpr auto window_height = 600;

void update_transforms( camera& a_camera )
{
    a_camera.projection_matrix = glm::perspective
    (
        glm::radians( a_camera.field_of_view_degrees ),
        (float) window_width / (float) window_height,
        0.1f,
        10000.0f
    );
    a_camera.direction =
    {
        std::cos( glm::radians( a_camera.vert_angle_degrees ) ) * std::sin( glm::radians( a_camera.horiz_angle_degrees ) ),
        std::sin( glm::radians( a_camera.vert_angle_degrees ) ),
        std::cos( glm::radians( a_camera.vert_angle_degrees ) ) * std::cos( glm::radians( a_camera.horiz_angle_degrees ) )
    };
    a_camera.right =
    {
        std::sin( glm::radians( a_camera.horiz_angle_degrees - 90.0f ) ),
        0.0f,
        std::cos( glm::radians( a_camera.horiz_angle_degrees - 90.0f ) )
    };
    a_camera.up = glm::cross( a_camera.right, a_camera.direction );
    a_camera.view_matrix = glm::lookAt
    (
        a_camera.position,
        a_camera.position + a_camera.direction,
        a_camera.up
    );
}

void process_mouse_move_event( camera& a_camera,
                               const SDL_MouseMotionEvent& a_ev,
                               float a_delta_time )
{
    if( a_ev.x == window_width / 2 && a_ev.y == window_height / 2 )
        return;
    a_camera.horiz_angle_degrees += a_camera.mouse_speed * a_delta_time * -float( a_ev.xrel );
    a_camera.vert_angle_degrees += a_camera.mouse_speed * a_delta_time * -float( a_ev.yrel );
}

void draw_hello_triangle( const camera& a_camera,
                          icgl::gl::render_state& a_state,
                          const glm::mat4& a_model_matrix,
                          icgl::shader_program& a_shader )
{
    a_state.set_shader_program( &a_shader );
    a_shader.bind();
    a_shader.set_uniform
    (
        a_shader.get_uniform( "MVP" ), 
        a_camera.projection_matrix * a_camera.view_matrix * a_model_matrix
    );
    icgl::draw_sample_triangle( a_state );
}

void draw_model_sample( const camera& a_camera,
                        icgl::gl::render_state& a_state,
                        const glm::mat4& a_model_matrix,
                        icgl::shader_program& a_shader,
                        icgl_test::obj_file& a_obj )
{
    a_state.set_shader_program( &a_shader );
    a_shader.bind();

    a_shader.set_uniform
    (
        a_shader.get_uniform( "MVP" ),
        a_camera.projection_matrix * a_camera.view_matrix * a_model_matrix 
    );
    a_shader.set_uniform( a_shader.get_uniform( "model" ), a_model_matrix );
    a_state.set_vertex_format( &a_obj.vf );
    a_state.set_vertex_buffer( &a_obj.vb );
    a_state.update_gl_state();

    glDrawArrays( GL_TRIANGLES, 0, a_obj.vertex_count );
}

void draw_light( const camera& a_camera,
                 icgl::gl::render_state& a_state,
                 const glm::mat4& a_model_matrix,
                 icgl::shader_program& a_shader,
                 icgl_test::obj_file& a_model )
{
    a_shader.set_uniform( a_shader.get_uniform( "MVP" ), 
                          a_camera.projection_matrix * a_camera.view_matrix * a_model_matrix );
    a_state.set_shader_program( &a_shader );
    a_state.set_vertex_format( &a_model.vf );
    a_state.set_vertex_buffer( &a_model.vb );
    a_state.update_gl_state();

    glDrawArrays( GL_TRIANGLES, 0, a_model.vertex_count );
}

[[nodiscard]] std::string quick_read_file( std::filesystem::path a_path )
{
    const auto size = std::filesystem::file_size( a_path );
    std::string data{};
    data.resize( size );
    auto stream = std::ifstream{a_path};
    stream.read( data.data(), size );
    return data;
}

namespace sc = std::chrono;

int main( int argc, char** argv )
{
    SDL_Init( SDL_INIT_VIDEO );
    app_support::window window{ "ICGL Test", window_width, window_height };
    SDL_SetRelativeMouseMode( SDL_TRUE );
    bool done{false};
    if( gladLoadGLLoader( SDL_GL_GetProcAddress ) == 0 )
        return -1;
    glEnable( GL_CULL_FACE );
    glEnable( GL_DEPTH_TEST );
    glDepthMask( GL_TRUE );
    icgl::gl::render_state state;
    /*try
    {*/
        icgl::shader_program shad{ vs, fs };
        icgl::shader_program light_shad{light_vs, light_fs};
        icgl::shader_program lit_object_shad{lit_object_vs, lit_object_fs};
    /*}
    catch( const std::exception& ex )
    {
        std::printf( "Failed to compile shader.\n" );
        std::printf( "%s\n", ex.what() );
    }*/
    camera render_camera{};
    auto last_frame_time = sc::high_resolution_clock::now();
    bool should_lock_mouse = true;
    std::printf( "%ls\n", std::filesystem::current_path().c_str() );
    const auto model_filename = "./sam.obj";
    const auto model_source = quick_read_file( model_filename );
    const auto cube_model_source = quick_read_file( "./cube.obj" ); 
    auto model = icgl_test::load_obj( model_source );
    auto cube_model = icgl_test::load_obj( cube_model_source );
    const auto scene_filename = argv[1] ? argv[1] : "./sam.scene";
    const auto scene_source = quick_read_file( scene_filename );
    const auto scene_items = icgl_test::scene_format::parse_scene_format( scene_source );
    std::map< std::string, icgl_test::obj_file > meshname_to_object{};
    for( const auto& si : scene_items )
    {
        if( si.type_str == "meshref" )
        {
            const auto& mesh_name = si.get_value( "mesh" );
            const auto& mesh_path = std::get< std::string >( mesh_name.data );
            if( meshname_to_object.contains( mesh_path ) )
                continue;
            const auto mesh_source = quick_read_file( mesh_path );
            meshname_to_object[mesh_path] = icgl_test::load_obj( mesh_source );
        }
    }
    while( !done )
    {
        const auto delta_time = sc::high_resolution_clock::now() - last_frame_time;
        const auto delta_time_ms = 
            (float) sc::duration_cast< sc::nanoseconds >( delta_time ).count() / 1000.0f;
        SDL_Event ev;
        while( SDL_PollEvent( &ev ) )
        {
            switch( ev.type )
            {
                case SDL_EventType::SDL_QUIT:
                    done = true;
                    break;
                case SDL_EventType::SDL_KEYDOWN:
                {
                    if( ev.key.keysym.sym == SDLK_F8 )
                    {
                        should_lock_mouse = !should_lock_mouse;
                        SDL_SetRelativeMouseMode( should_lock_mouse ? SDL_TRUE : SDL_FALSE );
                    }
                    render_camera.position += process_camera_move_event( render_camera, ev.key, delta_time_ms );
                    break;
                }
                case SDL_EventType::SDL_MOUSEMOTION:
                    process_mouse_move_event( render_camera, ev.motion, delta_time_ms );
                    break;
                default:
                    break;
            }
        }
        icgl::clear( 0.0f, 0.0f, 0.0f );
        update_transforms( render_camera );
        for( const auto& si : scene_items )
        {
            if( si.type_str == "meshref" )
            {
                const auto meshname = std::get< std::string >( si.get_value( "mesh" ).data );
                const auto position = std::get< glm::vec3 >( si.get_value( "pos" ).data );
                const auto matrix = glm::translate( glm::mat4{1.0f}, position );
                draw_model_sample( render_camera, 
                                   state,
                                   matrix,
                                   lit_object_shad,
                                   meshname_to_object[meshname] );
            }
        }
        draw_hello_triangle( render_camera,
                             state,
                             glm::mat4{1.0f},
                             shad );
        lit_object_shad.set_uniform( lit_object_shad.get_uniform( "object_color" ), {0.1, 0.6, 0.1, 1.0} );
        lit_object_shad.set_uniform( lit_object_shad.get_uniform( "light_color" ), {1.0, 1.0, 1.0, 1.0} );
        lit_object_shad.set_uniform( lit_object_shad.get_uniform( "light_pos" ), {2.0, 1.0, 2.0, 1.0} );
        draw_model_sample( render_camera,
                           state,
                           glm::mat4{1.0f},
                           lit_object_shad,
                           model );
        draw_light( render_camera, 
                    state,
                    glm::scale( glm::translate( glm::mat4{1.0f}, {2.0f, 1.0f, 2.0f} ), {0.2f, 0.2f, 0.2f} ),
                    light_shad,
                    cube_model );
        window.swap_window();
        last_frame_time = std::chrono::high_resolution_clock::now();
    }
    return 0;
}
