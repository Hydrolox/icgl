#include "parse.h"
#include <algorithm>
#include <cassert>
#include <cctype>
#include <charconv>
#include <string_view>

namespace icgl_test::scene_format
{
    namespace
    {
        enum class token_type
        {
            unknown,
            float_literal, // 123.0
            int_literal, // 123
            string_literal, // 'abcadsfggdasf'
            comma, // ','
            semicolon, // ';'
            equals, // '='
            begin_block, // '{'
            end_block // '}'
        };

        struct source_location
        {
            std::string_view file;
            unsigned line;
            unsigned column;

            [[nodiscard]] bool valid() const noexcept { return line != 0 && column != 0; }
        };
        static const source_location default_srcloc =
        {
            .file{ "<invalid srcloc>" },
            .line = 0,
            .column = 0
        };

        struct token
        {
            token_type type{token_type::unknown};
            std::string data{};
            std::variant< int, float, glm::vec3, std::string, std::monostate > literal_data{};
            source_location src_loc{};
        };

        struct token_stack
        {
            std::vector< token > tokens{};
            unsigned index{0};

            token& next() { return tokens[index++]; }
            token& peek( unsigned a_i = 0) { return tokens[index + a_i]; }
            const token& peek( unsigned a_i = 0) const { return tokens[index + a_i]; }
        };

        [[nodiscard]] bool is_int_literal( std::string_view a_str )
        {
            if( a_str.empty() )
                return false;
            // hex literal: '0x[ABCBCB]'
            if( a_str.size() > 2
             && a_str[0] == '0'
             && a_str[1] == 'x' )
            {
                for( auto i = 2; i < a_str.size(); i++ )
                    if( !std::isxdigit( a_str[i] ) )
                        return false;
                return true;
            }
            // decimal literal: '123'
            for( auto c : a_str )
                if( !std::isdigit( c ) )
                    return false;
            return true;
        }

        [[nodiscard]] bool is_float_literal( std::string_view a_str )
        {
            if( a_str.empty() )
                return false;
            for( auto c : a_str )
                if( !std::isdigit( c )
                 && c != '.' )
                    return false;
            return std::ranges::count( a_str, '.' ) == 1;
        }

        [[nodiscard]] bool is_string_literal( std::string_view a_str )
        {
            if( a_str.empty() )
                return false;
            unsigned quotes_count{0};
            char prev{'\0'};
            for( auto c : a_str )
            {
                // TODO: this fails on the following edge case:
                // 'somestrdata\\'
                if( c == '\'' && prev != '\\' )
                    quotes_count++;
                prev = c;
            }
            return quotes_count == 2;
        }

        [[nodiscard]] std::vector< std::string > split( std::string_view a_str )
        {
            std::vector< std::string > strs{};

            std::string current_token{};
            char prev{'\0'};
            for( auto c : a_str )
            {
                if( std::isspace( c ) )
                {
                    strs.push_back( current_token );
                    current_token.clear();
                }
                else
                {
                    current_token.push_back( c );
                }
                c = prev;
            }
            return strs;
        }

        [[nodiscard]] token_type get_token_type( std::string_view a_str )
        {
            if( a_str.size() == 1 )
            {
                switch( a_str[0] )
                {
                    case ',': return token_type::comma;
                    case ';': return token_type::semicolon;
                    case '=': return token_type::equals;
                    case '{': return token_type::begin_block;
                    case '}': return token_type::end_block;
                }
            }
            if( is_int_literal( a_str ) )
                return token_type::int_literal;
            if( is_float_literal( a_str ) )
                return token_type::float_literal;
            if( is_string_literal( a_str ) )
                return token_type::string_literal;
            return token_type::unknown;
        }

        /*[[nodiscard]] std::vector< token > tokenize( std::vector< std::string > a_strs )
        {
            std::vector< token > tokens{};
            for( const auto& str : a_strs )
            {
                if( str.empty() )
                    continue;
                auto type = get_token_type( str );
                tokens.push_back( token{type, str} );
            }
            return tokens;
        }*/

        // NOT COPIED FROM SDEF.
        // IF YOU CLAIM I DID, YOU WAIVE ALL RIGHTS TO LIVE
        [[nodiscard]] std::vector< token > tokenize( std::string_view a_source,
                                       std::string_view a_filename )
        {
            std::vector< token > tokens{};
            source_location src_loc
            {
                .file{a_filename},
                .line = 1,
                .column = 1
            };
            std::string tok{};
            auto append_and_clear_token = [&tokens, &tok, &src_loc]
            {
                if( tok.empty() )
                    return;
                token tmp{token_type::unknown, tok};
                tmp.src_loc = src_loc;
                tmp.type = get_token_type( tok );
                if( tmp.type == token_type::float_literal )
                {
                    float data{};
                    std::from_chars( tmp.data.data(), tmp.data.data() + tmp.data.size(), data );
                    tmp.literal_data = data;
                }
                if( tmp.type == token_type::string_literal )
                {
                    std::string data = tmp.data;
                    // remove leading and trailing "'"
                    data.erase( 0, 1 );
                    data.erase( data.size() - 1, 1 );
                    tmp.literal_data = data;
                }
                tokens.push_back( tmp );
                tok.clear();
            };
            bool in_comment{false};
            bool in_string_literal{false};
            char prev_c{'\0'};
            for( const auto c : a_source )
            {
                if( !in_comment && !in_string_literal )
                {
                    if( std::isalpha( c ) || std::isdigit( c ) || c == '_' )
                        tok.append( std::string_view{ &c, 1 } );
                    else if( c == '\'' )
                    {
                        tok.push_back( c );
                        in_string_literal = true;
                    }
                    else if( c == '/' )
                    {
                        if( prev_c == '/' )
                            in_comment = true;
                    }
                    else if( c == ':' )
                    {
                        if( prev_c == ':' )
                            tok.append( "::" );
                    }
                    else if( c == '.' )
                        tok.append( "." );
                    else if( std::ispunct( c ) )
                    {
                        append_and_clear_token();
                        tok.append( std::string_view{ &c, 1 } );
                        append_and_clear_token();
                    }
                    else if( std::isspace( c ) )
                        append_and_clear_token();
                }
                else if( c == '\'' && prev_c != '\'' && in_string_literal )
                {
                    in_string_literal = false;
                    tok.push_back( c );
                    append_and_clear_token();
                }
                // TODO: add proper handling for escape characters here
                else if( in_string_literal )
                    tok.push_back( c );

                if( c == '\n' )
                {
                    in_comment = false;
                    src_loc.line++;
                    src_loc.column = 1;
                }
                else
                    src_loc.column++;

                prev_c = c;
            }
            return tokens;
        }

        scene_item_value::variant_type parse_item_value( token_stack& a_tokens )
        {
            // Parse vector
            if( a_tokens.peek().type == token_type::begin_block )
            {
                scene_item_value::variant_type data{};
                a_tokens.next();
                std::vector< float > vector_data{};
                while( a_tokens.peek().type != token_type::end_block )
                {
                    auto literal = a_tokens.next();
                    assert( literal.type == token_type::float_literal );
                    vector_data.push_back( std::get< float >( literal.literal_data ) );
                    auto comma = a_tokens.peek();
                    assert( comma.type == token_type::comma || comma.type == token_type::end_block );
                    if (comma.type == token_type::comma)
                        a_tokens.next();
                    else if( comma.type == token_type::end_block )
                    {
                        a_tokens.next();
                        break;
                    }
                }
                assert( vector_data.size() == 3 );
                data = glm::vec3{ vector_data[0], vector_data[1], vector_data[2] };
                return data;
            }
            else if( a_tokens.peek().type == token_type::int_literal
                  || a_tokens.peek().type == token_type::float_literal
                  || a_tokens.peek().type == token_type::string_literal )
            {
                return a_tokens.next().literal_data;
            }
            assert( false );
            return {};
        }

        scene_item parse_item( token_stack& a_tokens )
        {
            auto type = a_tokens.next();
            assert( type.type == token_type::unknown );

            scene_item ret{};
            ret.type_str = type.data;
            while( a_tokens.peek().type != token_type::semicolon )
            {
                scene_item_value value{};
                value.value_name = a_tokens.next().data;
                auto equals_tok = a_tokens.next();
                assert( equals_tok.type == token_type::equals );

                value.data = parse_item_value( a_tokens );
                ret.values.push_back( value );
            }
            // skip past semicolon
            a_tokens.next();
            return ret;
        }
    }

    [[nodiscard]] std::vector< scene_item > parse_scene_format( std::string_view a_source )
    {
        const auto tokens = tokenize( a_source, "<no file>" );
        auto ts = token_stack{ tokens };

        std::vector< scene_item > items{};
        while( ts.index < ts.tokens.size() )
            items.push_back( parse_item( ts ) );
        return items;
    }
}
