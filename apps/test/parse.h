#ifndef ICGL_APPS_TEST_PARSE_H
#define ICGL_APPS_TEST_PARSE_H
#include <glm/vec3.hpp>
#include <stdexcept>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace icgl_test::scene_format
{
    struct scene_item_value
    {
        using variant_type = std::variant< int, float, glm::vec3, std::string, std::monostate >;
        variant_type data{};
        std::string value_name{};
    };

    struct scene_item
    {
        enum class item_type
        {
            none,
            cube,
            sphere,
            meshref,
            light,
        };

        std::vector< scene_item_value > values{};
        item_type type{item_type::none};
        std::string type_str{};

        const scene_item_value& get_value( std::string_view a_name ) const
        {
            for( const auto& val : values )
                if( val.value_name == a_name )
                    return val;
            throw std::range_error{"could not find scene item value"};
        }
    };

    [[nodiscard]] std::vector< scene_item > parse_scene_format( std::string_view source );
}

#endif
