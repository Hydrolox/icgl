#ifndef ICGL_APPS_TEST_OBJLOAD_H
#define ICGL_APPS_TEST_OBJLOAD_H
#include <cstdlib>
#include <icgl/icgl_buffer.h>
#include <icgl/icgl_vertex_format.h>
#include <string>
#include <string_view>
#include <vector>

namespace icgl_test
{
    struct obj_file
    {
        icgl::gl::vertex_buffer vb{};
        icgl::gl::vertex_format vf{};
        std::size_t vertex_count{0};
        std::vector< float > vertex_data{};
        std::string object_name{};
    };

    [[nodiscard]] obj_file load_obj( std::string_view source );
}

#endif
