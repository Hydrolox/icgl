#include "objload.h"
#include <array>
#include <cctype>
#include <cstdio>
#include <glm/geometric.hpp>
#include <glm/vec3.hpp>
#include <icgl/icgl_vertex_format.h>
#include <sstream>
#include <string>

namespace icgl_test
{
    namespace
    {
        void trim_leading_whitespace( std::string& a_str )
        {
            int i{0};
            for( auto c : a_str )
            {
                if( !std::isspace( c ) )
                    break;
                i++;
            }
            a_str.erase( std::begin( a_str ), std::begin( a_str ) + i );
        }
    }

    [[nodiscard]] obj_file load_obj( std::string_view a_source )
    {
        obj_file ret{};
        ret.vf = icgl::gl::vertex_format
        {
            {icgl::gl::vertex_format::elem_type::float_, 3}, // position
            {icgl::gl::vertex_format::elem_type::float_, 3}  // normal
        };
        std::string source{a_source};
        std::vector< glm::vec3 > verts{};
        std::vector< glm::vec3 > normals{};
        std::vector< std::array< unsigned, 6 > > faces{}; // vec3<int> of vertex index, vec3<int> of normal index
        std::istringstream input{source};
        for( std::string line; std::getline( input, line );)
        {
            trim_leading_whitespace( line );
            if( line.empty() )
                continue;
            std::string line_contents{std::begin( line ) + 2, std::end( line )};
            if( line[0] == '#' )
                continue;
            else if( line[0] == 's' ) // We don't care about smooth shading for now
                continue;
            else if( line[0] == 'o' )
                ret.object_name = std::string{line_contents};
            else if( line[0] == 'v' )
            {
                if( line[1] == ' ' )
                {
                    std::istringstream line_data{line_contents};
                    float x{}, y{}, z{};
                    line_data >> x >> y >> z;
                    verts.push_back( glm::vec3{x, y, z} );
                }
                else if( line[1] == 'n' )
                {
                    std::istringstream line_data{line_contents};
                    float x{}, y{}, z{};
                    line_data >> x >> y >> z;
                    normals.push_back( {x,y,z} );
                }
            }
            else if( line[0] == 'f' )
            {
                std::istringstream line_data{line_contents};
                struct vertex_ref
                {
                    unsigned vertex_index{0};
                    unsigned texcoord{0};
                    unsigned normal_index{0};
                };
                // 1[/[2]/3]
                auto read_vertex_ref = [&line_data]() -> vertex_ref
                {
                    vertex_ref ret{};
                    line_data >> ret.vertex_index;
                    if( line_data.peek() != '/' )
                        return ret;
                    char slash{};
                    line_data >> slash;
                    // HACK: implement reading texcoords later
                    line_data >> slash;
                    assert( slash == '/' );
                    //line_data >> ret.texcoord;
                    line_data >> ret.normal_index;
                    return ret;
                };

                auto vertex1 = read_vertex_ref();
                auto vertex2 = read_vertex_ref();
                auto vertex3 = read_vertex_ref();
                faces.push_back
                (
                    {vertex1.vertex_index - 1, vertex2.vertex_index - 1, vertex3.vertex_index - 1,
                     vertex1.normal_index - 1, vertex2.normal_index - 1, vertex3.normal_index - 1}
                );
            }
            else
                std::printf( "unrecognized obj type \"%s\"!\n", line.c_str() );
        }
        for( const auto& face : faces )
        {
            auto push_vertex = [&ret]( const glm::vec3& a_vertex )
            {
                ret.vertex_data.push_back( a_vertex.x );
                ret.vertex_data.push_back( a_vertex.y );
                ret.vertex_data.push_back( a_vertex.z );
            };
            auto maybe_push_normal = [&ret]( const glm::vec3& a_normal, unsigned a_index )
            {
                if ( a_index == static_cast< unsigned >( -1 ) )
                {
                    for( auto i = 0; i < 3 * 3; i++ )
                        ret.vertex_data.push_back( 0.0 );
                    return;
                }
                auto normal = glm::normalize( a_normal );
                ret.vertex_data.push_back( normal.x );
                ret.vertex_data.push_back( normal.y );
                ret.vertex_data.push_back( normal.z );
            };
            push_vertex( verts[face[0]] );
            maybe_push_normal( normals[face[3]], face[3] );
            push_vertex( verts[face[1]] );
            maybe_push_normal( normals[face[4]], face[4] );
            push_vertex( verts[face[2]] );
            maybe_push_normal( normals[face[5]], face[5] );
        }
        ret.vertex_count = ret.vertex_data.size() / 6;
        ret.vb.resize( ret.vertex_count, 6 * sizeof( float ) );
        ret.vb.upload( ret.vertex_data.data() );
        return ret;
    }
}
