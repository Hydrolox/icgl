#include <SDL.h>
#include "as_sdl_error.h"

namespace app_support
{
    const char* sdl_error::what() const noexcept
    {
        return SDL_GetError();
    }
}
