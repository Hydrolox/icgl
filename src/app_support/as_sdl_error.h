#ifndef ICGL_APP_SUPPORT_SDL_ERROR_H
#define ICGL_APP_SUPPORT_SDL_ERROR_H
#include <exception>

namespace app_support
{
    class sdl_error :
          public std::exception
    {
    public:
        virtual const char* what() const noexcept override;
    };
}

#endif
