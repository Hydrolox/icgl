#ifndef ICGL_APP_SUPPORT_WINDOW_H
#define ICGL_APP_SUPPORT_WINDOW_H
#include <SDL.h>
#include <SDL_video.h>
#include "as_sdl_error.h"

namespace app_support
{
    class window
    {
    public:
        window() = default;
        window( const char* a_title,
                int a_width,
                int a_height,
                int a_x = SDL_WINDOWPOS_UNDEFINED,
                int a_y = SDL_WINDOWPOS_UNDEFINED ) :
                window_{SDL_CreateWindow( a_title,
                                          a_x,
                                          a_y,
                                          a_width,
                                          a_height,
                                          SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE )}
        {
            if( window_ == nullptr )
                throw sdl_error{};
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 0 );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
            gl_context = SDL_GL_CreateContext( window_ );
        }
        ~window()
        {
            if( window_ )
                SDL_DestroyWindow( window_ );
        }
        void swap_window()
        {
            SDL_GL_SwapWindow( window_ );
        }
        [[nodiscard]] bool is_focused() const noexcept { return SDL_GetWindowFlags( window_ ) & SDL_WINDOW_INPUT_FOCUS; }
        [[nodiscard]] SDL_Window* get_window() const noexcept { return window_; }
    private:
        SDL_Window* window_{nullptr};
        SDL_GLContext gl_context{nullptr};
    };
}

#endif
