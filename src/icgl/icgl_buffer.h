#ifndef ICGL_ICGL_ICGL_BUFFER_H
#define ICGL_ICGL_ICGL_BUFFER_H
#include <cstdlib>
#include <utility>
#include "private/icgl_gl_include.h"

namespace icgl::gl
{
    class vertex_buffer
    {
    public:
        friend void swap( vertex_buffer& a_a, vertex_buffer& a_b )
        {
            using std::swap;
            swap( a_a.buffer, a_b.buffer );
            swap( a_a.vertex_array, a_b.vertex_array );
            swap( a_a.starting_offset, a_b.starting_offset );
            swap( a_a.vertex_count, a_b.vertex_count );
            swap( a_a.stride, a_b.stride );
        }
        vertex_buffer() noexcept;
        vertex_buffer( vertex_buffer&& a_other ) { swap( *this, a_other ); }
        vertex_buffer( const vertex_buffer& ) = delete;
        vertex_buffer& operator=( const vertex_buffer& ) = delete;
        vertex_buffer& operator=( vertex_buffer&& a_other ) { swap( *this, a_other ); return *this; }
        ~vertex_buffer() noexcept;
        void resize( std::size_t vertex_count, std::size_t stride_override ) noexcept;
        void upload( const void* data ) noexcept;
        [[nodiscard]] GLuint get_buffer() const noexcept { return buffer; }
        [[nodiscard]] GLuint get_vertex_array() const noexcept { return vertex_array; }
    private:
        GLuint buffer{0};
        GLuint vertex_array{0};
        std::size_t starting_offset{0};
        std::size_t vertex_count{0};
        std::size_t stride{0};
    };
}

#endif
