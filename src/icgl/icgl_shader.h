#ifndef ICGL_ICGL_ICGL_SHADER_H
#define ICGL_ICGL_ICGL_SHADER_H
#include <exception>
#include <glm/mat4x4.hpp>
#include <string>
#include <string_view>
#include <utility>
#include "private/icgl_gl_include.h"

namespace icgl
{
    class shader_compilation_failure :
          public std::exception
    {
    public:
        shader_compilation_failure( GLuint a_shader, bool a_link_failure, int a_info_log_length ) :
            shader{a_shader}
        {
            error.resize( info_log_length + 1 );
            if( !link_failure )
                glGetShaderInfoLog( shader, info_log_length, nullptr, error.data() );
            else
                glGetProgramInfoLog( shader, info_log_length, nullptr, error.data() );
        }
        virtual const char* what() const noexcept override;
    private:
        GLuint shader{0};
        bool link_failure{};
        int info_log_length{0};
        mutable std::string error{};
    };

    class shader_program
    {
    public:
        friend void swap( shader_program& a_a, shader_program& a_b )
        {
            using std::swap;
            swap( a_a.vs, a_b.vs );
            swap( a_a.fs, a_b.fs );
            swap( a_a.program, a_b.program );
        }
        shader_program() = default;
        shader_program( const shader_program& ) = delete;
        shader_program( shader_program&& a_other ) { swap( *this, a_other ); }
        shader_program& operator=( const shader_program& ) = delete;
        shader_program& operator=( shader_program&& a_other ) { swap( *this, a_other ); return *this; }
        shader_program( const char* vs, const char* fs );
        [[nodiscard]] GLuint get_program_id() const noexcept { return program; }
        void bind() const noexcept { glUseProgram( program ); }
        GLuint get_uniform( const char* name ) const;
        void set_uniform( GLuint uniform, const glm::vec4& vec );
        void set_uniform( GLuint uniform, const glm::mat4& mat );
    private:
        GLuint vs{0};
        GLuint fs{0};
        GLuint program{0};
    };
}

#endif
