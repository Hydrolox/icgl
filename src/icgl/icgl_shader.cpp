#include <fmt/core.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <stdexcept>
#include "icgl/icgl_shader.h"
#include "icgl_shader.h"

namespace icgl
{
    const char* shader_compilation_failure::what() const noexcept
    {
        return error.c_str();
    }

    shader_program::shader_program( const char* a_vs_src, const char* a_fs_src )
    {
        vs = glCreateShader( GL_VERTEX_SHADER );
        fs = glCreateShader( GL_FRAGMENT_SHADER );

        auto check_compile = []( GLuint a_shader, bool a_linking = false )
        {
            GLint compile_succeeded{GL_FALSE};
            int info_log_length{0};

            if( a_linking )
            {
                glGetProgramiv( a_shader, GL_LINK_STATUS, &compile_succeeded );
                glGetProgramiv( a_shader, GL_INFO_LOG_LENGTH, &info_log_length );
            }
            else
            {
                glGetShaderiv( a_shader, GL_COMPILE_STATUS, &compile_succeeded );
                glGetShaderiv( a_shader, GL_INFO_LOG_LENGTH, &info_log_length );
            }
            if( !compile_succeeded && info_log_length )
                throw shader_compilation_failure{a_shader, a_linking, info_log_length};
        };
        auto compile = [check_compile]( GLuint a_shader, const char* a_source )
        {
            glShaderSource( a_shader, 1, &a_source, nullptr );
            glCompileShader( a_shader );
            //check_compile( a_shader );
        };
        compile( vs, a_vs_src );
        compile( fs, a_fs_src );

        program = glCreateProgram();
        glAttachShader( program, vs );
        glAttachShader( program, fs );
        glLinkProgram( program );
        check_compile( program, true );
    }

    GLuint shader_program::get_uniform( const char* a_name ) const
    {
        auto uniform = glGetUniformLocation( program, a_name );
        if( uniform == -1 )
            throw std::invalid_argument{ fmt::format( "Uniform \"{}\" is invalid/does not exist.", a_name ) };
        return uniform;
    }

    void shader_program::set_uniform( GLuint a_uniform, const glm::vec4& a_vec )
    {
        glUseProgram( program );
        glUniform4fv( a_uniform, 1, glm::value_ptr( a_vec ) );
    }

    void shader_program::set_uniform( GLuint a_uniform, const glm::mat4& a_mat )
    {
        glUseProgram( program );
        glUniformMatrix4fv( a_uniform, 1, GL_FALSE, glm::value_ptr( a_mat ) );
    }
}
