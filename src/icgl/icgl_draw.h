#ifndef ICGL_ICGL_ICGL_DRAW_H
#define ICGL_ICGL_ICGL_DRAW_H

namespace icgl
{
    namespace gl
    {
        class render_state;
    }
    void clear( float red, float green, float blue, float alpha = 1.0f );
    void draw_sample_triangle( gl::render_state& state );
}

#endif
