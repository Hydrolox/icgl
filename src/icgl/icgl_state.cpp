#include "icgl_state.h"
#include <cassert>
#include <icgl/private/icgl_gl_include.h>
#include <icgl/icgl_buffer.h>
#include <icgl/icgl_shader.h>
#include <icgl/icgl_state.h>
#include <icgl/icgl_vertex_format.h>


namespace icgl::gl
{
    void render_state::set_shader_program( shader_program* a_program )
    {
        program = a_program;
        program_dirty = true;
    }

    void render_state::set_vertex_buffer( vertex_buffer* a_vb )
    {
        vertex_buffer_ = a_vb;
        vertex_buffer_dirty = true;
    }

    void render_state::set_vertex_format( vertex_format* a_vf )
    {
        vertex_format_ = a_vf;
        vertex_format_dirty = true;
    }

    void render_state::update_gl_state()
    {
        if( program_dirty )
            glUseProgram( program ? program->get_program_id() : 0 );
        if( vertex_buffer_dirty )
        {
            glBindBuffer( GL_ARRAY_BUFFER, vertex_buffer_ ? vertex_buffer_->get_buffer() : 0 );
            glBindVertexArray( vertex_buffer_ ? vertex_buffer_->get_vertex_array() : 0 );
        }
        if( vertex_format_dirty )
        {
            for( auto i = 0, offset = 0; const auto& e : vertex_format_->get_elements() )
            {
                glEnableVertexAttribArray( i );
                glVertexAttribPointer
                (
                    i,
                    e.vector_size,
                    gl_enum( e.type ),
                    GL_FALSE,
                    vertex_format_->get_size(),
                    reinterpret_cast< const void* >( offset )
                );
                offset += elem_type_size( e.type ) * e.vector_size;
                i++;
            }
        }
    }
}
