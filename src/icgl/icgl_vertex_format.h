#ifndef ICGL_ICGL_ICGL_VERTEX_FORMAT_H
#define ICGL_ICGL_ICGL_VERTEX_FORMAT_H
#include <icgl/private/icgl_gl_include.h>
#include <initializer_list>
#include <span>
#include <vector>

namespace icgl::gl
{
    class vertex_format
    {
    public:
        enum class elem_type
        {
            unknown,
            float_,
            ushort_,
            int_,
        };

        struct elem
        {
            elem_type type{elem_type::unknown};
            unsigned vector_size{1};
        };

        vertex_format() = default;
        vertex_format( vertex_format&& ) = default;
        vertex_format( const vertex_format& ) = default;
        vertex_format( std::initializer_list< elem > a_elems ) noexcept
            : elements( a_elems )
        {
            recalculate_size();
        }
        vertex_format& operator=( const vertex_format& ) = default;
        vertex_format& operator=( vertex_format&& ) = default;
        ~vertex_format() = default;

        [[nodiscard]] auto get_elements() const noexcept { return std::span{elements}; }
        [[nodiscard]] auto get_size() const noexcept { return size; }

    private:
        void recalculate_size();

        std::vector< elem > elements{};
        unsigned size{0};
    };

    [[nodiscard]] GLenum gl_enum( vertex_format::elem_type et ) noexcept;
    [[nodiscard]] std::size_t elem_type_size( vertex_format::elem_type et ) noexcept;
}

#endif
