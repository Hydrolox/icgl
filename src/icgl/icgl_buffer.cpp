#include "icgl_buffer.h"
#include <cstdio>

namespace icgl::gl
{
    vertex_buffer::vertex_buffer() noexcept
    {
        glGenBuffers( 1, &buffer );
        glBindBuffer( buffer, GL_ARRAY_BUFFER );
        glBindBuffer( 0, GL_ARRAY_BUFFER );
        glGenVertexArrays( 1, &vertex_array );
    }

    vertex_buffer::~vertex_buffer() noexcept
    {
        glDeleteBuffers( 1, &buffer );
    }

    void vertex_buffer::resize( std::size_t a_vertex_count, std::size_t a_stride ) noexcept
    {
        stride = a_stride;
        vertex_count = a_vertex_count;
    }

    void vertex_buffer::upload( const void* a_data ) noexcept
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer );
        glBufferData( GL_ARRAY_BUFFER, vertex_count * stride, a_data, GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
    }
}
