#include "icgl_draw.h"
#include <mutex>
#include <icgl/icgl_buffer.h>
#include <icgl/icgl_state.h>
#include <icgl/icgl_vertex_format.h>
#include "private/icgl_gl_include.h"

namespace icgl
{

    void clear( float a_red, float a_green, float a_blue, float a_alpha )
    {
        glClearColor( a_red, a_green, a_blue, a_alpha );
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    }

    void draw_sample_triangle( gl::render_state& a_state )
    {
        static const GLfloat vb_data[] =
        {
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            0.0f, 1.0f, 0.0f
        };

        glEnable( GL_DEBUG_OUTPUT );

        static gl::vertex_buffer* vb;
        static gl::vertex_format vf =
        {
            { gl::vertex_format::elem_type::float_, 3 }
        };
        if( !vb )
        {
            vb = new gl::vertex_buffer();
            vb->resize( std::size( vb_data ), sizeof( GLfloat ) );
            vb->upload( vb_data );
        }

        a_state.set_vertex_format( &vf );
        a_state.set_vertex_buffer( vb );
        a_state.update_gl_state();

        glDrawArrays( GL_TRIANGLES, 0, 3 );
    }

}
