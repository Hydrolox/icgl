#include "icgl_vertex_format.h"
#include "icgl/icgl_vertex_format.h"
#include <cassert>


namespace  icgl::gl
{
    void vertex_format::recalculate_size()
    {
        size = 0;
        for( const auto& elem : elements )
            size += elem_type_size( elem.type ) * elem.vector_size;
    }

    [[nodiscard]] GLenum gl_enum( vertex_format::elem_type a_et ) noexcept
    {
        using enum vertex_format::elem_type;
        switch( a_et )
        {
            case float_:
                return GL_FLOAT;
            case int_:
                return GL_INT;
            case ushort_:
                return GL_UNSIGNED_SHORT;
            case unknown:
                assert( false );
        }
        assert( false );
        return 0;
    }

    [[nodiscard]] std::size_t elem_type_size( vertex_format::elem_type a_et ) noexcept
    {
        using enum vertex_format::elem_type;
        switch( a_et )
        {
            case float_:
                return sizeof( GLfloat );
            case int_:
                return sizeof( GLint );
            case ushort_:
                return sizeof( GLushort );
            case unknown:
                assert( false );
        }
        assert( false );
        return 0;
    }
}
