#ifndef ICGL_ICGL_ICGL_STATE_H
#define ICGL_ICGL_ICGL_STATE_H
#include <array>
#include <cstdlib>

namespace icgl
{
    class shader_program;
}

namespace icgl::gl
{
    class vertex_buffer;
    class vertex_format;

    class render_state
    {
    public:
        render_state() = default;
        render_state( const render_state& ) = delete;
        render_state( render_state&& ) = delete;
        render_state& operator=( const render_state& ) = delete;
        render_state& operator=( render_state&& ) = delete;
        ~render_state() = default;
        void set_shader_program( shader_program* program );
        void set_vertex_buffer( vertex_buffer* vb );
        void set_vertex_format( vertex_format* vf );
        void update_gl_state();
    private:
        bool program_dirty{true};
        shader_program* program{nullptr};

        bool vertex_buffer_dirty{true};
        vertex_buffer* vertex_buffer_{nullptr};

        bool vertex_format_dirty{true};
        vertex_format* vertex_format_{nullptr};
    };
}

#endif
